package de.sim2908.wotclanscraper.clan;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@FieldDefaults(level = PRIVATE, makeFinal = true)
class ClanServiceTest {

    private final static String TEST_TAG = "test";
    private final static Integer TEST_UID = 123456;
    private final static String TEST_URL = "https://api.worldoftanks.eu/wot/clans/list/?application_id=null&fields=clan_id&search=test";

    ClanRepository clanRepository = mock(ClanRepository.class);
    RestTemplate restTemplate = mock(RestTemplate.class);
    ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

    ObjectMapper objectMapper = new ObjectMapper();

    ClanService clanService = new ClanService(clanRepository, restTemplate, objectMapper);

    @Test
    void getClanUidFromRepository() throws ClanFetchException {
        when(clanRepository.existsById(TEST_TAG)).thenReturn(true);
        when(clanRepository.getOne(TEST_TAG)).thenReturn(new Clan(TEST_TAG, TEST_UID));

        final var clanUid = clanService.getClanUid(TEST_TAG);

        assertThat(clanUid, is(TEST_UID));

        verify(clanRepository).existsById(TEST_TAG);
        verify(clanRepository).getOne(TEST_TAG);
    }

    @Test
    void getClanUidFromAPIwithTagInRepository() throws ClanFetchException {
        when(clanRepository.existsById(TEST_TAG)).thenReturn(true);
        when(clanRepository.getOne(TEST_TAG)).thenReturn(new Clan(TEST_TAG, null));
        when(restTemplate.getForEntity(TEST_URL, String.class)).thenReturn(responseEntity);
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        when(responseEntity.getBody()).thenReturn("{\"data\":[{\"clan_id\": " + TEST_UID + "}]}");

        final var clanUid = clanService.getClanUid(TEST_TAG);

        assertThat(clanUid, is(TEST_UID));

        verify(clanRepository).existsById(TEST_TAG);
        verify(clanRepository).getOne(TEST_TAG);
        verify(restTemplate).getForEntity(TEST_URL, String.class);
        verify(responseEntity).getStatusCode();
        verify(responseEntity).getBody();
    }

    @Test
    void getClanUidFromAPIwithTagNotInRepository() throws ClanFetchException {
        when(clanRepository.existsById(TEST_TAG)).thenReturn(false);
        when(restTemplate.getForEntity(TEST_URL, String.class)).thenReturn(responseEntity);
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);
        when(responseEntity.getBody()).thenReturn("{\"data\":[{\"clan_id\": " + TEST_UID + "}]}");

        final var clanUid = clanService.getClanUid(TEST_TAG);

        assertThat(clanUid, is(TEST_UID));

        verify(clanRepository).existsById(TEST_TAG);
        verify(restTemplate).getForEntity(TEST_URL, String.class);
        verify(responseEntity).getStatusCode();
        verify(responseEntity).getBody();
    }

    @Test
    void throwClanFetchExceptionOnNon2xxResponse() {
        when(clanRepository.existsById(TEST_TAG)).thenReturn(false);
        when(restTemplate.getForEntity(TEST_URL, String.class)).thenReturn(responseEntity);
        when(responseEntity.getStatusCode()).thenReturn(HttpStatus.NOT_FOUND);

        assertThrows(ClanFetchException.class, () -> clanService.getClanUid(TEST_TAG));

        verify(clanRepository).existsById(TEST_TAG);
        verify(restTemplate).getForEntity(TEST_URL, String.class);
        verify(responseEntity).getStatusCode();

    }


    @Test
    void getListOfClanTags() {
        when(clanRepository.findAll()).thenReturn(
                List.of(new Clan("A", 1),
                        new Clan("B", 2),
                        new Clan("C", 3),
                        new Clan("D", 5)));

        final var listOfClanTags = clanService.getListOfClanTags();

        assertThat(listOfClanTags, is(List.of("A", "B", "C", "D")));

        verify(clanRepository).findAll();
    }
}