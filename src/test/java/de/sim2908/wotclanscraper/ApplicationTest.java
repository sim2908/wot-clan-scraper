package de.sim2908.wotclanscraper;

import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static lombok.AccessLevel.PRIVATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureTestDatabase
@FieldDefaults(level = PRIVATE)
class ApplicationTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Test
    void isHealthy() {
        final var response = restTemplate.getForEntity("/actuator/health", String.class);
        assertThat(response.getStatusCodeValue(), is(200));
        assertThat(response.getBody(), is("{\"status\":\"UP\"}"));
    }

}
