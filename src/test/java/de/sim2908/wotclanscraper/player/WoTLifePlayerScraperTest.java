package de.sim2908.wotclanscraper.player;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class WoTLifePlayerScraperTest {

    private static final String URL_TEMPLATE = "https://wot-life.com/eu/player/%s";
    private static final String PLAYER_NAME = "SimIsMalding";

    Connection connection = mock(Connection.class);

    WoTLifePlayerScraper woTLifePlayerScraper = new WoTLifePlayerScraper();

    @Test
    void shouldScrapeData() throws IOException {
        when(connection.ignoreContentType(true)).thenReturn(connection);
        when(connection.get()).thenReturn(Jsoup.parse(getTestHtml()));

        try (MockedStatic<Jsoup> connectionMockedStatic = Mockito.mockStatic(Jsoup.class)) {
            connectionMockedStatic.when(() -> Jsoup.connect(format(URL_TEMPLATE, PLAYER_NAME)))
                    .thenReturn(connection);

            final var player = woTLifePlayerScraper.scrapeData(PLAYER_NAME);

            assertThat(player.getName(), is(PLAYER_NAME));
            assertThat(player.getBattles_overall(), is(13881));
            assertThat(player.getBattles_30d(), is(157));
            assertThat(player.getWn8_overall(), is(1398));
            assertThat(player.getWn8_30d(), is(2818));

            verify(connection).get();
            verifyNoMoreInteractions(connection);
        }
    }

    private String getTestHtml() throws IOException {
        return Files.readString(Path.of("src/test/resources/player/wot-life_player_test.html"));
    }
}