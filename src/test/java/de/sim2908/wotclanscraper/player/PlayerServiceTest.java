package de.sim2908.wotclanscraper.player;

import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static lombok.AccessLevel.PRIVATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@FieldDefaults(level = PRIVATE, makeFinal = true)
class PlayerServiceTest {
    private static final String PLAYER_NAME = "test-playername";

    PlayerRepository playerRepository = mock(PlayerRepository.class);
    PlayerScraper playerScraper = mock(PlayerScraper.class);
    Player player = mock(Player.class);

    PlayerService playerService = new PlayerService(playerRepository, playerScraper);

    @Test
    void shoudFetchPlayerFromDatabase() throws IOException {
        when(playerRepository.existsById(PLAYER_NAME)).thenReturn(true);
        when(playerRepository.getOne(PLAYER_NAME)).thenReturn(player);

        final var result = playerService.fetchPlayer(PLAYER_NAME);

        assertThat(result, is(player));
        verify(playerRepository).existsById(PLAYER_NAME);
        verify(playerRepository).getOne(PLAYER_NAME);
        verifyNoMoreInteractions(playerRepository);
        verifyNoInteractions(playerScraper);
        verifyNoInteractions(player);
    }

    @Test
    void shoudFetchPlayerFromScraper() throws IOException {
        when(playerRepository.existsById(PLAYER_NAME)).thenReturn(false);
        when(playerScraper.scrapeData(PLAYER_NAME)).thenReturn(player);

        final var result = playerService.fetchPlayer(PLAYER_NAME);

        assertThat(result, is(player));
        verify(playerRepository).existsById(PLAYER_NAME);
        verify(playerRepository).save(player);
        verifyNoMoreInteractions(playerRepository);
        verify(playerScraper).scrapeData(PLAYER_NAME);
        verifyNoMoreInteractions(playerScraper);
        verifyNoInteractions(player);
    }


    @Test
    @Disabled
    void shouldRemoveOldPlayerdata() {
        // This would basically test if my implementation
        // of "minus X days" and the repo works correctly.
        // Such testing is out-of-scope/not cost-effective for this Project.
    }
}