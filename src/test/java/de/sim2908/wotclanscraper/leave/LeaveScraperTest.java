package de.sim2908.wotclanscraper.leave;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sim2908.wotclanscraper.clan.ClanFetchException;
import de.sim2908.wotclanscraper.clan.ClanService;
import lombok.experimental.FieldDefaults;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static lombok.AccessLevel.PRIVATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@FieldDefaults(level = PRIVATE, makeFinal = true)
class LeaveScraperTest {
    private final static String TEST_TAG = "test";
    private final static Integer TEST_UID = 123456;

    ClanService clanService = mock(ClanService.class);
    Connection connection = mock(Connection.class);
    Document document = mock(Document.class);
    ObjectMapper objectMapper = new ObjectMapper();

    LeaveScraper leaveScraper = new LeaveScraper(clanService, objectMapper);

    @Test
    void shouldScrapeLeaves() throws LeaveFetchException, ClanFetchException, IOException {
        when(clanService.getClanUid(TEST_TAG)).thenReturn(TEST_UID);
        when(connection.ignoreContentType(true)).thenReturn(connection);
        when(connection.get()).thenReturn(document);
        when(document.html()).thenReturn(getTestHtml());

        try (MockedStatic<Jsoup> connectionMockedStatic = Mockito.mockStatic(Jsoup.class)) {
            connectionMockedStatic.when(() -> Jsoup.connect(any())).thenReturn(connection);

            final var leaves = leaveScraper.scrapeLeaves(TEST_TAG);

            assertThat(leaves.size(), is(4));

            final var member1 = leaves.get(0);
            assertThat(member1.getUid(), is(1));
            assertThat(member1.getName(), is("Member1"));
            assertThat(member1.getClan(), is(TEST_TAG));
            assertThat(member1.getRole(), is("private"));

            final var member2 = leaves.get(1);
            assertThat(member2.getUid(), is(2));
            assertThat(member2.getName(), is("Member2"));
            assertThat(member2.getClan(), is(TEST_TAG));
            assertThat(member2.getRole(), is("private"));

            final var member3 = leaves.get(2);
            assertThat(member3.getUid(), is(3));
            assertThat(member3.getName(), is("Member3"));
            assertThat(member3.getClan(), is(TEST_TAG));
            assertThat(member3.getRole(), is("private"));

            final var officer4 = leaves.get(3);
            assertThat(officer4.getUid(), is(4));
            assertThat(officer4.getName(), is("Officer4"));
            assertThat(officer4.getClan(), is(TEST_TAG));
            assertThat(officer4.getRole(), is("officer"));
        }
        verify(clanService).getClanUid(TEST_TAG);
        verify(connection).ignoreContentType(true);
        verify(connection).get();
        verify(document).html();
    }

    @Test
    void shouldThrowOnWebIOError() throws ClanFetchException, IOException {
        when(clanService.getClanUid(TEST_TAG)).thenReturn(TEST_UID);
        when(connection.ignoreContentType(true)).thenReturn(connection);
        when(connection.get()).thenThrow(new IOException());

        try (MockedStatic<Jsoup> connectionMockedStatic = Mockito.mockStatic(Jsoup.class)) {
            connectionMockedStatic.when(() -> Jsoup.connect(any())).thenReturn(connection);

            Assertions.assertThrows(LeaveFetchException.class, () ->
                    leaveScraper.scrapeLeaves(TEST_TAG));
        }

        verify(clanService).getClanUid(TEST_TAG);
        verify(connection).ignoreContentType(true);
        verify(connection).get();
    }

    private String getTestHtml() throws IOException {
        return Files.readString(Path.of("src/test/resources/leave/leave_test.html"));
    }

}