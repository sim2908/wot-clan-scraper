package de.sim2908.wotclanscraper.leave;

import de.sim2908.wotclanscraper.clan.ClanFetchException;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static lombok.AccessLevel.PRIVATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@FieldDefaults(level = PRIVATE, makeFinal = true)
class LeaveServiceTest {
    private final static String TEST_TAG = "test";

    LeaveRepository leaveRepository = mock(LeaveRepository.class);
    LeaveScraper leaveScraper = mock(LeaveScraper.class);
    LeaveService leaveService = new LeaveService(leaveScraper, leaveRepository);

    @SuppressWarnings("unchecked")
    ArgumentCaptor<ArrayList<Leave>> captor = ArgumentCaptor.forClass(ArrayList.class);

    @Test
    void shouldUpdateLeaves() throws LeaveFetchException, ClanFetchException {
        final var leaveList = buildLeavesList();
        when(leaveScraper.scrapeLeaves(TEST_TAG)).thenReturn(leaveList);

        leaveService.updateLeaves(TEST_TAG);

        verify(leaveRepository).saveAll(captor.capture());
        final var leaves = captor.getValue();

        assertThat(leaves, is(leaveList));
        verifyNoMoreInteractions(leaveRepository);
    }

    @Test
    @Disabled
    void shouldGetLeavesSince() {
        // This would basically test if my implementation
        // of "minus X days" and the repo works correctly.
        // Such testing is out-of-scope/not cost-effective for this Project.
    }

    @Test
    void shouldGetLeavesByClan() {
        when(leaveRepository.findByTimeBetween(any(), any())).thenReturn(buildLeavesList());

        final var leavesByClan = leaveService.getLeavesByClan(1);

        assertThat(leavesByClan.size(), is(3));
        assertThat(leavesByClan.get("clan1"), is(4));
        assertThat(leavesByClan.get("clan2"), is(8));
        assertThat(leavesByClan.get("clan3"), is(1));

        verify(leaveRepository).findByTimeBetween(any(), any());
    }

    private List<Leave> buildLeavesList() {
        final var leaves = new ArrayList<Leave>();

        leaves.addAll(buildLeavesForClan("clan1", 4));
        leaves.addAll(buildLeavesForClan("clan2", 8));
        leaves.addAll(buildLeavesForClan("clan3", 1));

        return leaves;
    }

    private List<Leave> buildLeavesForClan(String tag, int amount) {
        return Stream.generate(() -> new Leave(null, null, null, null, tag))
                .limit(amount)
                .collect(Collectors.toList());
    }
}