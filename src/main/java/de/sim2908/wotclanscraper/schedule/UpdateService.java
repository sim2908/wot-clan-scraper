package de.sim2908.wotclanscraper.schedule;

import de.sim2908.wotclanscraper.clan.ClanFetchException;
import de.sim2908.wotclanscraper.clan.ClanService;
import de.sim2908.wotclanscraper.leave.LeaveFetchException;
import de.sim2908.wotclanscraper.leave.LeaveService;
import de.sim2908.wotclanscraper.player.PlayerService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class UpdateService {

    @NonFinal
    @Value("${player.fetchOffset:7}")
    Integer fetchOffset;

    LeaveService leaveService;
    ClanService clanService;
    PlayerService playerService;

    @Scheduled(cron = "0 0 */2 * * *")
    public void onSchedule() {
        scrapeAllClans();
        removeOldPlayerdata();
        fetchNewPlayerdata();
    }

    public void scrapeAllClans() {
        final var start = now();
        log.info("Started scheduled scraping at {}", start);

        clanService.getListOfClanTags().forEach(tag -> {
            try {
                leaveService.updateLeaves(tag);
            } catch (LeaveFetchException | ClanFetchException e) {
                e.printStackTrace();
            }
        });

        final var end = now();
        final var elapsed = SECONDS.between(start, end);
        log.info("Finished scheduled scraping at {}. Time elapsed: {}s", end, elapsed);
    }

    private void removeOldPlayerdata() {
        final var start = now();
        log.info("Started scheduled removal of old Playerdata at {}", start);

        playerService.removeOldPlayerdata();

        final var end = now();
        final var elapsed = SECONDS.between(start, end);
        log.info("Finished scheduled removal of old Playerdata at {}. Time elapsed: {}s", end, elapsed);

    }

    public void fetchNewPlayerdata() {
        leaveService.getLeavesSince(fetchOffset).forEach(leave -> {
            try {
                playerService.fetchPlayer(leave.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }
}
