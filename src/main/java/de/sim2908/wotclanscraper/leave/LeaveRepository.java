package de.sim2908.wotclanscraper.leave;

import de.sim2908.wotclanscraper.leave.Leave.LeaveKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface LeaveRepository extends JpaRepository<Leave, LeaveKey> {
    // start = yesterday, end = today
    List<Leave> findByTimeBetween(Date start, Date end);
}
