package de.sim2908.wotclanscraper.leave;

import de.sim2908.wotclanscraper.clan.ClanFetchException;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;
import static java.time.ZoneOffset.UTC;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class LeaveService {
    LeaveScraper leaveScraper;
    LeaveRepository leaveRepository;

    public void updateLeaves(String tag) throws LeaveFetchException, ClanFetchException {
        final var leaves = leaveScraper.scrapeLeaves(tag);
        leaveRepository.saveAll(leaves);
    }

    public List<Leave> getLeavesSince(Integer days) {
        final var offset = LocalDateTime.now()
                .withHour(0).withMinute(0).withSecond(0)
                .withNano(0).minusDays(days)
                .toInstant(UTC)
                .toEpochMilli();

        return leaveRepository.findByTimeBetween(
                new Date(offset),
                new Date(currentTimeMillis()));
    }

    public Map<String, Integer> getLeavesByClan(int days) {
        final var leavesList = getLeavesSince(days);
        final var leavesMap = new HashMap<String, Integer>();
        leavesList.forEach(leave -> leavesMap.compute(leave.getClan(), (k, v) -> v == null ? 1 : v + 1));

        return leavesMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

}
