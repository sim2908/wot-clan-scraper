package de.sim2908.wotclanscraper.leave;

public class LeaveFetchException extends Exception {
    public LeaveFetchException(String cause, Exception e) {
        super(cause, e);
    }
}
