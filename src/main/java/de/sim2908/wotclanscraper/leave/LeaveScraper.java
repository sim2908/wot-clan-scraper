package de.sim2908.wotclanscraper.leave;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sim2908.wotclanscraper.clan.ClanFetchException;
import de.sim2908.wotclanscraper.clan.ClanService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class LeaveScraper {

    private static final String URL_TEMPLATE = "https://eu.wargaming.net/clans/wot/%s/newsfeed/api/events/?date_until=%s&offset=7200";
    private static final String TIMESTAMP_TEMPLATE = "%d-%d-%dT00%%3A00%%3A00%%2B00%%3A00";

    ClanService clanService;
    ObjectMapper objectMapper;

    public List<Leave> scrapeLeaves(String tag) throws LeaveFetchException, ClanFetchException {
        try {
            final var document = Jsoup.connect(buildUrl(tag)).ignoreContentType(true).get();
            return extractLeaves(removeHtmlWrapper(document.html()), tag);
        } catch (IOException e) {
            throw new LeaveFetchException("Could not access Wargaming clan page for: " + tag, e);
        }
    }

    private String buildUrl(String clanTag) throws ClanFetchException {
        final var clanUid = clanService.getClanUid(clanTag);
        final var timestamp = buildTimestamp();
        return format(URL_TEMPLATE, clanUid, timestamp);
    }

    private List<Leave> extractLeaves(String jsonString, String tag) throws LeaveFetchException {
        try {
            final var jsonNode = objectMapper.readTree(jsonString);
            final var leaves = new ArrayList<Leave>();

            jsonNode.get("items").elements().forEachRemaining(node -> {
                if (fieldAsText(node, "subtype").equals("leave_clan")) {
                    final var time = fieldAsText(node, "created_at").split("T")[0];
                    final var date = Date.valueOf(time);

                    node.get("accounts_info").fields().forEachRemaining(info -> {
                        final var uid = info.getKey();
                        final var name = fieldAsText(info.getValue(), "name");
                        final var role = fieldAsText(node.get("additional_info").get(uid).get(0), "last_role_name");

                        leaves.add(new Leave(Integer.valueOf(uid), date, name, role, tag));
                    });
                }
            });
            log.debug("Found {} Leaves for Clan {}", leaves.size(), tag);
            return leaves;
        } catch (IOException e) {
            throw new LeaveFetchException("Could not create JsonNode from response: " + jsonString, e);
        }
    }

    private String removeHtmlWrapper(String wrappedJson) {
        return wrappedJson.substring(32, wrappedJson.length() - 16);
    }

    private String buildTimestamp() {
        final var date = LocalDateTime.now();
        final var year = date.getYear();
        final var month = date.getMonth().getValue();
        final var day = date.getDayOfMonth();

        return String.format(TIMESTAMP_TEMPLATE, year, month, day);
    }

    private String fieldAsText(JsonNode node, String fieldName) {
        return node.get(fieldName).asText();
    }
}
