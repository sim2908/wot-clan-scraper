package de.sim2908.wotclanscraper.leave;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;

import static lombok.AccessLevel.PRIVATE;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "leave")
@IdClass(Leave.LeaveKey.class)
@FieldDefaults(level = PRIVATE)
public class Leave {

    @Id
    Integer uid;
    @Id
    Date time;
    String name;
    String role;
    String clan;

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    @FieldDefaults(level = PRIVATE)
    public static class LeaveKey implements Serializable {
        Integer uid;
        Date time;
    }
}
