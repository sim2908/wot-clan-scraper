package de.sim2908.wotclanscraper.leave;

import de.sim2908.wotclanscraper.player.Player;
import de.sim2908.wotclanscraper.player.PlayerService;
import de.sim2908.wotclanscraper.schedule.UpdateService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@Controller
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class LeaveController {

    LeaveService leaveService;
    PlayerService playerService;
    UpdateService updateService;

    @GetMapping(value = "/wcs/playerlist", produces = TEXT_HTML_VALUE)
    public String getPlayerListHtml(Model model,
                                    @RequestParam(value = "days") Integer days,
                                    @RequestParam(value = "wn8", required = false) Optional<Integer> wn8,
                                    @RequestParam(value = "maxwn8", required = false) Optional<Integer> maxwn8,
                                    @RequestParam(value = "recentwn8", required = false) Optional<Integer> recentwn8,
                                    @RequestParam(value = "maxrecentwn8", required = false) Optional<Integer> maxrecentwn8,
                                    @RequestParam(value = "recentbattles", required = false) Optional<Integer> recentbattles) {
        if (days <= 0) {
            throw new IllegalArgumentException("days <= 0");
        }
        model.addAttribute("players", buildPlayerList(days, wn8, maxwn8, recentwn8, maxrecentwn8, recentbattles));
        return "playerlist";
    }

    @ResponseBody
    @PostMapping(value = "/wcs/forceupdate")
    public void forceUpdateLeaves() {
        updateService.scrapeAllClans();
    }

    private List<Player> buildPlayerList(Integer days,
                                         Optional<Integer> minwn8Opt,
                                         Optional<Integer> maxwn8Opt,
                                         Optional<Integer> minrecentwn8Opt,
                                         Optional<Integer> maxrecentwn8Opt,
                                         Optional<Integer> recentbattlesOpt) {
        final var leaves = leaveService.getLeavesSince(days);

        final var minwn8 = minwn8Opt.orElse(0);
        final var minrecentwn8 = minrecentwn8Opt.orElse(0);
        final var recentbattles = recentbattlesOpt.orElse(0);
        final var maxwn8 = maxwn8Opt.orElse(0);
        final var maxrecentwn8 = maxrecentwn8Opt.orElse(0);

        return leaves.stream()
                .map(leave -> {
                    try {
                        final var player = playerService.fetchPlayer(leave.getName());
                        player.setClanTag(leave.getClan());
                        return player;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(player -> player != null &&
                        player.getWn8_overall() >= minwn8 &&
                        player.getWn8_30d() >= minrecentwn8 &&
                        player.getBattles_30d() >= recentbattles &&
                        (maxwn8 == 0 || player.getWn8_overall() <= maxwn8) &&
                        (maxrecentwn8 == 0 || player.getWn8_30d() <= maxrecentwn8))
                .collect(toList());
    }
}
