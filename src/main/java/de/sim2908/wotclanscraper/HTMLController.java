package de.sim2908.wotclanscraper;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@Controller
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class HTMLController {
    @GetMapping(value = {"/help", "/wcs/help", "/error"}, produces = TEXT_HTML_VALUE)
    public String getHelpPage() {
        return "readme";
    }

    @GetMapping(value = {"/wcs", "/wcs/player"}, produces = TEXT_HTML_VALUE)
    public String getPlayerLeavesFrontendPage(Model model,
                                              @RequestParam(value = "days", required = false) Integer days,
                                              @RequestParam(value = "wn8", required = false) Integer wn8,
                                              @RequestParam(value = "maxwn8", required = false) Integer maxwn8,
                                              @RequestParam(value = "recentwn8", required = false) Integer recentwn8,
                                              @RequestParam(value = "maxrecentwn8", required = false) Integer maxrecentwn8,
                                              @RequestParam(value = "recentbattles", required = false) Integer recentbattles) {
        model.addAttribute("days", days);
        model.addAttribute("wn8", wn8);
        model.addAttribute("maxwn8", maxwn8);
        model.addAttribute("recentwn8", recentwn8);
        model.addAttribute("maxrecentwn8", maxrecentwn8);
        model.addAttribute("recentbattles", recentbattles);

        return "playerLeaveFrontend";
    }

    @GetMapping(value = {"/wcs/clan"}, produces = TEXT_HTML_VALUE)
    public String getClanLeavesFrontendPage(Model model,
                                            @RequestParam(value = "days", required = false) Integer days) {

        model.addAttribute("days", days);

        return "clanLeaveFrontend";
    }
}
