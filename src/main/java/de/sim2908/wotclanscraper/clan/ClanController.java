package de.sim2908.wotclanscraper.clan;

import de.sim2908.wotclanscraper.leave.LeaveService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@Controller
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ClanController {

    ClanService clanService;
    LeaveService leaveService;

    @ResponseBody
    @GetMapping(value = "/wcs/clan/list", produces = APPLICATION_JSON_VALUE)
    public List<String> getClans() {
        return clanService.getListOfClanTags();
    }

    @GetMapping(value = "/wcs/clan/leaves", produces = TEXT_HTML_VALUE)
    public String getPlayerListHtml(Model model,
                                    @RequestParam(value = "days") Integer days) {
        if (days <= 0) {
            throw new IllegalArgumentException("days <= 0");
        }

        final var leaves = leaveService.getLeavesByClan(days);

        model.addAttribute("total", leaves.size());
        model.addAttribute("clans", leaves);

        return "clanlist";
    }
}
