package de.sim2908.wotclanscraper.clan;

public class ClanFetchException extends Exception {
    public ClanFetchException(String cause, Exception e) {
        super(cause, e);
    }

    public ClanFetchException(String cause) {
        super(cause);
    }
}
