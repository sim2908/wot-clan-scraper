package de.sim2908.wotclanscraper.clan;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClanRepository extends JpaRepository<Clan, String> {
}
