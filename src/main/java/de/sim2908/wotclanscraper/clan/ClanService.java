package de.sim2908.wotclanscraper.clan;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ClanService {

    private static final String URL_TEMPLATE =
            "https://api.worldoftanks.eu/wot/clans/list/?application_id=%s&fields=clan_id&search=%s";
    ClanRepository clanRepository;
    RestTemplate restTemplate;
    ObjectMapper objectMapper;
    @NonFinal
    @Value("${wargaming.token}")
    String wgToken;

    public Integer getClanUid(String tag) throws ClanFetchException {
        if (clanRepository.existsById(tag)) {
            final var clan = clanRepository.getOne(tag);
            final var clanUid = clan.getUid();
            if (clanUid != null) {
                return clanUid;
            }
        }
        return fetchClan(tag);
    }

    public List<String> getListOfClanTags() {
        return clanRepository.findAll().stream().map(Clan::getTag).collect(toList());
    }

    private Integer fetchClan(String tag) throws ClanFetchException {
        log.info("Fetching UID for Clan {}", tag);
        final var response = restTemplate.getForEntity(buildUrl(tag), String.class);
        if (response.getStatusCode().is2xxSuccessful()) {
            final var clan = responseToClan(response.getBody(), tag);
            log.debug("Saving UID {} for Clan {}", tag, clan.getUid());
            clanRepository.save(clan);
            return clan.getUid();
        }
        throw new ClanFetchException("API returned non 2xx statuscode: " + response);
    }

    private String buildUrl(String tag) {
        return format(URL_TEMPLATE, wgToken, tag);
    }

    private Clan responseToClan(String body, String tag) throws ClanFetchException {
        try {
            final var clanUid = nodeToId(objectMapper.readTree(body));
            return new Clan(tag, clanUid);
        } catch (JsonProcessingException e) {
            throw new ClanFetchException("Could not convert API-response to JsonNode: " + body, e);
        } catch (NullPointerException e) {
            throw new ClanFetchException("Could not find Clan: " + tag, e);
        }
    }

    private Integer nodeToId(JsonNode node) {
        final var data = node.get("data");
        final var clan = data.get(0);
        final var clanUid = clan.get("clan_id");
        return clanUid.asInt();
    }
}
