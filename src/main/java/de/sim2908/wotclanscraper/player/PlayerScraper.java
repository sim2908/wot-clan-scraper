package de.sim2908.wotclanscraper.player;

import java.io.IOException;

public interface PlayerScraper {

    Player scrapeData(String playerName) throws IOException;

}
