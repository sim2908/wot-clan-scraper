package de.sim2908.wotclanscraper.player;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PlayerService {

    @NonFinal
    @Value("${player.removalOffset:4}")
    Integer removalOffset;

    PlayerRepository playerRepository;
    PlayerScraper playerScraper;

    public Player fetchPlayer(String playerName) throws IOException {
        if (playerRepository.existsById(playerName)) {
            return playerRepository.getOne(playerName);
        }
        final var player = playerScraper.scrapeData(playerName);
        playerRepository.save(player);
        log.debug("Scraped and saved information about Player {}", playerName);
        return player;
    }

    public void removeOldPlayerdata() {
        final var date = new Date(now()
                .minusDays(removalOffset)
                .toInstant(UTC)
                .toEpochMilli());

        final var removed = playerRepository.removePlayersByUpdatedAtBefore(date);

        log.info("Removed {} expired playerdata entries", removed);
    }
}
