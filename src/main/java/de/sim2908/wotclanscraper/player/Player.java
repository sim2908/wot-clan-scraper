package de.sim2908.wotclanscraper.player;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Date;

import static lombok.AccessLevel.PRIVATE;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "player")
@FieldDefaults(level = PRIVATE)
public class Player {

    @Id
    String name;

    @CreationTimestamp
    @Column(name = "updated_at")
    Date updatedAt;

    @Transient
    @Setter
    String clanTag;

    Integer battles_overall;
    Integer battles_30d;
    Integer wn8_overall;
    Integer wn8_30d;

    @Deprecated
    String winrate_30d;
    @Deprecated
    String winrate_overall;
}
