package de.sim2908.wotclanscraper.player;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface PlayerRepository extends JpaRepository<Player, String> {
    long removePlayersByUpdatedAtBefore(Date before);
}
