package de.sim2908.wotclanscraper.player;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class WoTLifePlayerScraper implements PlayerScraper {

    private static final String URL_TEMPLATE = "https://wot-life.com/eu/player/%s";

    @Override
    public Player scrapeData(String playerName) throws IOException {
        final var document = Jsoup.connect(format(URL_TEMPLATE, playerName)).get();

        final var tab1 = document.getElementById("tab1");
        final var table = tab1.child(1).child(0);

        final var battles = table.child(1);

        final var battles_Overall = extractIntValue(battles, 1);
        final var battles_30d = extractIntValue(battles, 4);

        final var winRate = table.child(3);

        final var winRate_Overall = extractValue(winRate, 1);
        final var winRate_30d = extractValue(winRate, 8);

        final var wn8 = table.child(15);

        final var wn8_Overall = extractIntValue(wn8, 1);
        final var wn8_30d = extractIntValue(wn8, 4);

        return Player.builder()
                .name(playerName)
                .battles_overall(battles_Overall)
                .battles_30d(battles_30d)
                .winrate_overall(winRate_Overall)
                .winrate_30d(winRate_30d)
                .wn8_overall(wn8_Overall)
                .wn8_30d(wn8_30d)
                .build();
    }

    private Integer extractIntValue(Element element, Integer child) {
        return Double.valueOf(extractValue(element, child)).intValue();
    }

    private String extractValue(Element element, Integer child) {
        return element.child(child).childNode(0).toString().replace(",", ".");
    }
}
