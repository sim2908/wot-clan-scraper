create table leave
(
    uid integer not null,
    time date not null,
    name varchar(255) not null,
    role varchar(255) not null,
    primary key (uid, time)
);

create table clan
(
    tag varchar(255) not null
        primary key,
    uid integer
);