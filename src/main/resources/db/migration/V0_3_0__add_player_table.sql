create table player
(
    name            varchar(255) not null
        primary key,
    updated_at      date         not null,
    battles_overall integer      not null,
    battles_30d     integer      not null,
    winrate_overall varchar(255) not null,
    winrate_30d     varchar(255) not null,
    wn8_overall     integer      not null,
    wn8_30d         integer      not null
);