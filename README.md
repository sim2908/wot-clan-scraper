# WoT Clan Scraper

## Important notice

This project is currently in Version **1.2.0**!

If you encounter any issues please notify me on my [gitlab-project](https://gitlab.com/sim2908/wot-clan-scraper/-/issues) or directly through [email](mailto:wotcstool@gmail.com?subject=WotCSTool_Issue)

## Usage

If you are interested in using this tool or its code please contact me through [email](mailto:wotcstool@gmail.com?subject=WotCSTool_Usage).

## Help

If you are looking for documentation on how to use this please look at the help page under `/wcs/help`